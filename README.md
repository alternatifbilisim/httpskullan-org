
HTTPS Kullan
===================

**HTTPS Kullan:** Mahremiyet ve veri güvenliği için SSL kullanımının önemini; kullanıcılar ve web servisleri açısından vurgulamayı amaçlayan bir [Alternatif Bilişim Derneği](https://www.alternatifbilisim.org) projesidir. Bununla birlikte içerik, kullanıcının; "SSL'i tercih ve talep etmesi", web servisinin; "öntanımlı bağlantı şeklini güvenli protokol ile sağlaması" biçimlerinde aksiyon almasını hedefler. 

Mevcut depo, projenin web sayfasına ait dosyaları barındırır. 

Https Kullan: [https://httpskullan.org](https://httpskullan.org/)

#### Sertifika Bilgileri:

SHA-256 Fingerprint:

`2C:53:5C:78:16:E8:6E:26:17:28:85:C4:F7:86:4A:FF:D0:73:38:07:B6:D8:2E:67:16:E1:05:0A:70:42:0A:A4`

SHA1 Fingerprint:

`2E:9B:BF:90:EB:BC:8A:E3:84:C0:E1:6C:44:E7:0D:2B:50:68:3F:CB`

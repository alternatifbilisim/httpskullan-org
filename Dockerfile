FROM zzrot/alpine-caddy
MAINTAINER Alternatif Bilisim
# docker build -t altbil/httpskullan .
# docker run -d -p 80:80 altbil/httpskullan
RUN apk --no-cache add openssl
RUN wget https://github.com/AlternatifBilisim/httpskullan.org/archive/master.zip -O /tmp/master.zip
RUN unzip /tmp/master.zip -d /tmp
RUN mv /tmp/httpskullan.org-master /var/www/html
RUN rm -rf /tmp/master.zip
